import React, {useEffect, useState} from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import momentTz from 'moment-timezone';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import './calendar.scss'
import Modal from 'react-bootstrap/Modal'

const localizer = momentLocalizer(moment);
var gapi = window.gapi;
gapi.load('client:auth2', () => {
  gapi.client.init({
    apiKey: 'AIzaSyAGUmbmM1vizWFl7mOjGBaYVneNJ_0T8cw',
    clientId: '473711844112-r7if6kcpd5avaos698etg7ev40a4qojg.apps.googleusercontent.com',
    discoveryDocs:['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
    scope: 'https://www.googleapis.com/auth/calendar',
  })
  gapi.client.load('calendar', 'v3', () => console.log('loaded calendar'));
})


const CalendarContainer = () => {
  const[userLoggedIn, setUserLoggedIn] = useState(localStorage.getItem('user'));
  const[title, setTitle] = useState('');
  const[start, setStart] = useState('');
  const[end, setEnd] = useState('');
  const[events, setEvents] = useState([]);
  const[modal, setModal] = useState(false);
  const[description, setDescription] = useState('');

  useEffect(() => {
    if(userLoggedIn){
      getData();
    }
  }, [userLoggedIn]);

  const handlerClick = () => {
    gapi.auth2.getAuthInstance().signIn()
      .then((res) => {
        console.log(res);
        let {access_token} = res.xc
        gapi.client.setToken({access_token});
        localStorage.setItem('user',JSON.stringify({access_token}));
        setUserLoggedIn(localStorage.getItem('user'));
      })
  }

  const getData = () => {
    gapi.client.calendar.events.list({
      'calendarId': 'primary',
      'timeMin': (new Date()).toISOString(),
      'showDeleted': false,
      'singleEvents': true,
      'maxResults': 10,
      'orderBy': 'startTime'
    })
    .then(res => {
      if (res.result.items === []) {
        setEvents([''])
      } else {
        let tratada = [];
        res.result.items.map(item => {
          tratada.push({
            'title': item.summary,
            'start': item.start.date || item.start.dateTime,
            'end': item.end.date || item.start.dateTime,
          })
        });
        setEvents(tratada);
      }
    })
  }

  const signOut = () => {
    gapi.auth2.getAuthInstance().signOut();
    localStorage.setItem('user', '');
    setUserLoggedIn(localStorage.getItem('user'));
  }

  const createEvent = (evt) => {
    evt.preventDefault();

    const newEvent = {
      'summary': title,
      'location': '800 Howard St., San Francisco, CA 94103',
      'description': description,
      'start': {
        'dateTime': moment(start).format(),
        'timeZone': momentTz.tz.guess()
      },
      'end': {
        'dateTime': moment(end).format(),
        'timeZone': momentTz.tz.guess()
      },
      'recurrence': [
        'RRULE:FREQ=DAILY;COUNT=1'
      ],
      'attendees': [
        {'email': 'danisor1412@gmail.com'},
      ],
      'reminders': {
        'useDefault': false,
        'overrides': [
          {'method': 'email', 'minutes': 24 * 60},
          {'method': 'popup', 'minutes': 10}
        ]
      }
    };

    gapi.client.calendar.events.insert({
      calendarId: 'primary',
      resource: newEvent,
    }).execute();

    setModal(false);
  }

  return(
    <div className="row">
      <div className="col-md-12">
        <h1 className="text-center">Calendar</h1>
        {
          !userLoggedIn? 
          <div className="content">
            <button onClick={handlerClick} className="btn btn-primary my-3">Sign In</button>
          </div> 
          : 
          <div className="content">
            <div>
              <button variant="primary" onClick={() => setModal(true)} className="btn btn-primary">
                New Event
              </button>
              <a onClick={signOut}>
                Logout
              </a>
            </div>
            
          </div>
        }
        {
          !userLoggedIn?
          <h4 className="text-center">
            You need to log in to view your calendar
          </h4>
          :
          <div  style={{ height: '500pt'}}>
            <Calendar
              events={events}
              startAccessor="start"
              endAccessor="end"
              defaultDate={moment().toDate()}
              localizer={localizer}
            />
          </div>
        }
        
        <Modal show={modal} onHide={() => setModal(false)} animation={false}>
          <Modal.Header closeButton>
            <Modal.Title>Create new event</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form >
              <div className="form-group">
                <label htmlFor="name">Title</label>
                <input className="form-control" id="name" value={title} onChange={e => setTitle(e.target.value)} type="text"  placeholder="Enter event title"/>
              </div>
              <div className="form-group">
                <label htmlFor="name">Description</label>
                <input className="form-control" id="name" value={description} onChange={e => setDescription(e.target.value)} type="text"  placeholder="Enter a event description"/>
              </div>
              <div className="form-group">
                <label htmlFor="name">Start</label>
                <input className="form-control" id="name" value={start} onChange={e => setStart(e.target.value)} type="datetime-local" />
              </div>
              <div className="form-group">
                <label htmlFor="name">End</label>
                <input className="form-control" id="name" value={end} onChange={e => setEnd(e.target.value)} type="datetime-local" />
              </div>
              <button onClick={() => setModal(false)}>Close</button>
              <button onClick={createEvent}>Save Changes</button>
            </form>
          </Modal.Body>
        </Modal>
      </div>
    </div>
  )
}

export default CalendarContainer;
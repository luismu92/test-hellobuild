import React, { useState } from 'react';
import {loginWithGithub} from '../../config/firebase';
import {Tabs, Tab} from 'react-bootstrap'
import './github.scss';

const GithubContainer = () => {

    const [userName, setUserName] = useState('');
    const [avatar, setAvatar] = useState('');
    const [error, setError] = useState(null);
    const [listRepos, setListRepos] = useState([]);
  
    const handleClick = () => {
      loginWithGithub().then(user => {
        console.log(user.additionalUserInfo.profile)
        const profile = user.additionalUserInfo.profile;
        setRepoAndUser(profile);
        
      }).catch(err => {
        console.log(err)
        setError(err)
      })
    }
  
    async function setRepoAndUser(profile){
      setData(profile);
      setError(null)
      const responseListRepos = await fetch(profile.repos_url)
      const listRepositories = await responseListRepos.json();
      setDataListRepo(listRepositories)
    }

    const setData = ({ login, avatar_url }) => {
      setUserName(login);
      setAvatar(avatar_url);
    };
  
    const setDataListRepo = (listRepositories) => {
      setListRepos(listRepositories);
    };
  
  
    return(
      <div className="row justify-content-center">
        <div className="col-md-6">
          <h1 className="text-center">GitHub</h1>
          
          { userName === '' || listRepos.length === 0 ? 
            (
              <div className="content">
                <button onClick={handleClick} className="btn btn-primary my-3">Sign In</button>
  
                <h4 className="text-center">
                  You need to log in to view your github account
                </h4>
            {error ? (<h4>{error}</h4>): ''}
              </div>
            )  : (
              <div className="card">
                <div className="content-img">
                  <img src={avatar} alt=""/>
                </div>
                <div className="card-body">
                  <p className="text-center"><b><u>User</u></b></p>
                  <h3 className="card-title text-center">{userName}</h3>
                  <hr/>
                  <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                    <Tab eventKey="repositories" title="List of repositories">
                      <ul className="list-group">
                        {
                          listRepos.map((repo) =>
                             <li className="list-group-item" key={repo.full_name} >{repo.name}</li>
                          )
                        }
                      </ul>
                    </Tab>
                  </Tabs>
                  
                </div>
              </div>
            )
          }
          
        </div>
      </div>
    )
  }

export default GithubContainer;

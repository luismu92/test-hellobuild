import React, { useState } from 'react';
import Calendar from '../Calendar/CalendarContainer';
import Github from '../Github/GithubContainer';
import Home from '../Home/HomeContainer';
import NavBar from './navbar';

const Dashboard = () => {

  let view;
  const [tab, setTab] = useState('home');

  const navigate = (tabName) => {
    setTab(tabName)
  }
  
    switch (tab) {
      case 'calendar':
        view = <Calendar />
        break;
      
      case 'github':
        view = <Github />
        break;
      
      case 'home':
        view = <Home />
        break;
    
      default:
        view = <Home />
        break;
    }

  return(
    <div>
      <NavBar navigate={navigate}/>
      <div className="container">
        {view}
      </div>
    </div>
  )
}

export default Dashboard;
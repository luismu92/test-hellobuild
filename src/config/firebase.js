import firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyCnwOYbhh5xM-Rzr7Lx3o0C6xOwdkGopJI",
  authDomain: "test-hellobuild-510e7.firebaseapp.com",
  projectId: "test-hellobuild-510e7",
  storageBucket: "test-hellobuild-510e7.appspot.com",
  messagingSenderId: "434886488182",
  appId: "1:434886488182:web:2e66a9267fe34d94886f57",
  measurementId: "G-E5HZS1T9EN"
};

firebase.initializeApp(firebaseConfig);
export const loginWithGithub = () => {
  const githubProvider = new firebase.auth.GithubAuthProvider()
  return firebase.auth().signInWithPopup(githubProvider);
}

